usb_ml_protocol = Proto("USB_mystic_light",  "USB Mystic Light protocol")

local requestType	= ProtoField.uint8("usb_mystic_light.requestType",   "requestType  ", base.DEC)
local jrgb1			= ProtoField.bytes("usb_mystic_light.jrgb1",         "jrgb1        ", base.SPACE)
local jpipe1		= ProtoField.bytes("usb_mystic_light.jpipe1",        "jpipe1       ", base.SPACE)
local jpipe2		= ProtoField.bytes("usb_mystic_light.jpipe2",        "jpipe2       ", base.SPACE)
local jrainbow1		= ProtoField.bytes("usb_mystic_light.jrainbow1",     "jrainbow1    ", base.SPACE)
local jrainbow2		= ProtoField.bytes("usb_mystic_light.jrainbow2",     "jrainbow2    ", base.SPACE)
local jcorsair		= ProtoField.bytes("usb_mystic_light.jcorsair",      "jcorsair     ", base.SPACE)
local jcorsairOuter	= ProtoField.bytes("usb_mystic_light.jcorsairOuter", "jCorsairOuter", base.SPACE)
local onboard0		= ProtoField.bytes("usb_mystic_light.onboard0",      "onboard0     ", base.SPACE)
local onboard1		= ProtoField.bytes("usb_mystic_light.onboard1",      "onboard1     ", base.SPACE)
local onboard2		= ProtoField.bytes("usb_mystic_light.onboard2",      "onboard2     ", base.SPACE)
local onboard3		= ProtoField.bytes("usb_mystic_light.onboard3",      "onboard3     ", base.SPACE)
local onboard4		= ProtoField.bytes("usb_mystic_light.onboard4",      "onboard4     ", base.SPACE)
local onboard5		= ProtoField.bytes("usb_mystic_light.onboard5",      "onboard5     ", base.SPACE)
local onboard6		= ProtoField.bytes("usb_mystic_light.onboard6",      "onboard6     ", base.SPACE)
local onboard7		= ProtoField.bytes("usb_mystic_light.onboard7",      "onboard7     ", base.SPACE)
local onboard8		= ProtoField.bytes("usb_mystic_light.onboard8",      "onboard8     ", base.SPACE)
local onboard9		= ProtoField.bytes("usb_mystic_light.onboard9",      "onboard9     ", base.SPACE)
local onboard10		= ProtoField.bytes("usb_mystic_light.onboard10",     "onboard10    ", base.SPACE)
local jrgb2			= ProtoField.bytes("usb_mystic_light.jrgb2",         "jrgb2        ", base.SPACE)
local save			= ProtoField.uint8("usb_mystic_light.save",          "save         ", base.DEC)
local header		= ProtoField.bytes("usb_mystic_light.header",        "header    ", base.SPACE)
local led0_4		= ProtoField.bytes("usb_mystic_light.led0_4",        "led0_4    ", base.SPACE)
local led5_9		= ProtoField.bytes("usb_mystic_light.led5_9",        "led5_9    ", base.SPACE)
local led10_14		= ProtoField.bytes("usb_mystic_light.led10_14",      "led10_14  ", base.SPACE)
local led15_19		= ProtoField.bytes("usb_mystic_light.led15_19",      "led15_19  ", base.SPACE)
local led20_24		= ProtoField.bytes("usb_mystic_light.led20_24",      "led20_24  ", base.SPACE)
local led25_29		= ProtoField.bytes("usb_mystic_light.led25_29",      "led25_29  ", base.SPACE)
local led30_34		= ProtoField.bytes("usb_mystic_light.led30_34",      "led30_34  ", base.SPACE)
local led35_39		= ProtoField.bytes("usb_mystic_light.led35_39",      "led35_39  ", base.SPACE)
local led40_44		= ProtoField.bytes("usb_mystic_light.led40_44",      "led40_44  ", base.SPACE)
local led45_49		= ProtoField.bytes("usb_mystic_light.led45_49",      "led45_49  ", base.SPACE)
local led50_54		= ProtoField.bytes("usb_mystic_light.led50_54",      "led50_54  ", base.SPACE)
local led55_59		= ProtoField.bytes("usb_mystic_light.led55_59",      "led55_59  ", base.SPACE)
local led60_64		= ProtoField.bytes("usb_mystic_light.led60_64",      "led60_64  ", base.SPACE)
local led65_69		= ProtoField.bytes("usb_mystic_light.led65_69",      "led65_69  ", base.SPACE)
local led70_74		= ProtoField.bytes("usb_mystic_light.led70_74",      "led70_74  ", base.SPACE)
local led75_79		= ProtoField.bytes("usb_mystic_light.led75_79",      "led75_79  ", base.SPACE)
local led80_84		= ProtoField.bytes("usb_mystic_light.led80_84",      "led80_84  ", base.SPACE)
local led85_89		= ProtoField.bytes("usb_mystic_light.led85_89",      "led85_89  ", base.SPACE)
local led90_94		= ProtoField.bytes("usb_mystic_light.led90_94",      "led90_94  ", base.SPACE)
local led95_99		= ProtoField.bytes("usb_mystic_light.led95_99",      "led95_99  ", base.SPACE)
local led100_104	= ProtoField.bytes("usb_mystic_light.led100_104",    "led100_104", base.SPACE)
local led105_109	= ProtoField.bytes("usb_mystic_light.led105_109",    "led105_109", base.SPACE)
local led110_114	= ProtoField.bytes("usb_mystic_light.led110_114",    "led110_114", base.SPACE)
local led115_119	= ProtoField.bytes("usb_mystic_light.led115_119",    "led115_119", base.SPACE)
local led120_124	= ProtoField.bytes("usb_mystic_light.led120_124",    "led120_124", base.SPACE)
local led125_129	= ProtoField.bytes("usb_mystic_light.led125_129",    "led125_129", base.SPACE)
local led130_134	= ProtoField.bytes("usb_mystic_light.led130_134",    "led130_134", base.SPACE)
local led135_139	= ProtoField.bytes("usb_mystic_light.led135_139",    "led135_139", base.SPACE)
local led140_144	= ProtoField.bytes("usb_mystic_light.led140_144",    "led140_144", base.SPACE)
local led145_149	= ProtoField.bytes("usb_mystic_light.led145_149",    "led145_149", base.SPACE)
local led150_154	= ProtoField.bytes("usb_mystic_light.led150_154",    "led150_154", base.SPACE)
local led155_159	= ProtoField.bytes("usb_mystic_light.led155_159",    "led155_159", base.SPACE)
local led160_164	= ProtoField.bytes("usb_mystic_light.led160_164",    "led160_164", base.SPACE)
local led165_169	= ProtoField.bytes("usb_mystic_light.led165_169",    "led165_169", base.SPACE)
local led170_174	= ProtoField.bytes("usb_mystic_light.led170_174",    "led170_174", base.SPACE)
local led175_179	= ProtoField.bytes("usb_mystic_light.led175_179",    "led175_179", base.SPACE)
local led180_184	= ProtoField.bytes("usb_mystic_light.led180_184",    "led180_184", base.SPACE)
local led185_189	= ProtoField.bytes("usb_mystic_light.led185_189",    "led185_189", base.SPACE)
local led190_194	= ProtoField.bytes("usb_mystic_light.led190_194",    "led190_194", base.SPACE)
local led195_199	= ProtoField.bytes("usb_mystic_light.led195_199",    "led195_199", base.SPACE)
local led200_204	= ProtoField.bytes("usb_mystic_light.led200_204",    "led200_204", base.SPACE)
local led205_209	= ProtoField.bytes("usb_mystic_light.led205_209",    "led205_209", base.SPACE)
local led210_214	= ProtoField.bytes("usb_mystic_light.led210_214",    "led210_214", base.SPACE)
local led215_219	= ProtoField.bytes("usb_mystic_light.led215_219",    "led215_219", base.SPACE)
local led220_224	= ProtoField.bytes("usb_mystic_light.led220_224",    "led220_224", base.SPACE)
local led225_229	= ProtoField.bytes("usb_mystic_light.led225_229",    "led225_229", base.SPACE)
local led230_234	= ProtoField.bytes("usb_mystic_light.led230_234",    "led230_234", base.SPACE)
local led235_239	= ProtoField.bytes("usb_mystic_light.led235_239",    "led235_239", base.SPACE)

usb_ml_protocol.fields = { requestType, jrgb1, jpipe1, jpipe2, jrainbow1, jrainbow2, jcorsair, jcorsairOuter,
						   onboard0, onboard1, onboard2, onboard3, onboard4, onboard5, onboard6, onboard7, onboard8, onboard9, onboard10, jrgb2, save,
						   header, led0_4, led5_9, led10_14, led15_19, led20_24, led25_29, led30_34, led35_39, led40_44, led45_49, led50_54,  led55_59, 
						   led60_64, led65_69, led70_74, led75_79, led80_84, led85_89, led90_94, led95_99, led100_104, led105_109, led110_114, led115_119,
						   led120_124, led125_129, led130_134, led135_139, led140_144, led145_149, led150_154, led155_159, led160_164, led165_169, led170_174,
						   led175_179, led180_184, led185_189, led190_194, led195_199, led200_204, led205_209, led210_214, led215_219, led220_224, led225_229,
						   led230_234, led235_239 }

 
function usb_ml_protocol.dissector(buffer, pinfo, tree)
	local length = buffer:len()
	-- only accept expected data length
	if (length ~= 192) and (length ~= 185) and (length ~= 169) and (length ~= 162) and (length ~= 732) then return end
	
	-- detemrmine offset into data for interpretation
	local offset
	if (length == 192) or (length == 169) or (length == 732) then
		offset = 7
	else
		offset = 0
	end

	-- only accept requests 82 and 83 as Mystic Light
	if (tostring(buffer(offset,1)) ~= "52") and (tostring(buffer(offset,1)) ~= "53") then return end
	
	pinfo.cols.protocol = usb_ml_protocol.name

	if tostring(buffer(offset,1)) == "52" then
		-- handle OEM requests
		if (length == 192) or (length == 185) then
			-- handle 185 byte data
			local subtree = tree:add(usb_ml_protocol, buffer(offset), "USB Mystic Light 185")
	  
			subtree:add(requestType,   buffer(offset,        1))
			subtree:add(jrgb1,         buffer(offset + 1,   10))
			subtree:add(jpipe1,        buffer(offset + 11,  10))
			subtree:add(jpipe2,        buffer(offset + 21,  10))
			subtree:add(jrainbow1,     buffer(offset + 31,  11))
			subtree:add(jrainbow2,     buffer(offset + 42,  11))
			subtree:add(jcorsair,      buffer(offset + 53,  11))
			subtree:add(jcorsairOuter, buffer(offset + 64,  10))
			subtree:add(onboard0,      buffer(offset + 74,  10))
			subtree:add(onboard1,      buffer(offset + 84,  10))
			subtree:add(onboard2,      buffer(offset + 94,  10))
			subtree:add(onboard3,      buffer(offset + 104, 10))
			subtree:add(onboard4,      buffer(offset + 114, 10))
			subtree:add(onboard5,      buffer(offset + 124, 10))
			subtree:add(onboard6,      buffer(offset + 134, 10))
			subtree:add(onboard7,      buffer(offset + 144, 10))
			subtree:add(onboard8,      buffer(offset + 154, 10))
			subtree:add(onboard9,      buffer(offset + 164, 10))
			subtree:add(jrgb2,         buffer(offset + 174, 10))
			subtree:add(save,          buffer(offset + 184,  1))
		elseif (length == 169) or (length == 162) then
			-- handle 162 byte data
			local subtree = tree:add(usb_ml_protocol, buffer(7), "USB Mystic Light 162")

			subtree:add(requestType,   buffer(offset,        1))
			subtree:add(jrgb1,         buffer(offset + 1,   10))
			subtree:add(jrainbow1,     buffer(offset + 11,  10))
			subtree:add(jcorsair,      buffer(offset + 21,  10))
			subtree:add(jcorsairOuter, buffer(offset + 31,  10))
			subtree:add(onboard0,      buffer(offset + 41,  10))
			subtree:add(onboard1,      buffer(offset + 51,  10))
			subtree:add(onboard2,      buffer(offset + 61,  10))
			subtree:add(onboard3,      buffer(offset + 71,  10))
			subtree:add(onboard4,      buffer(offset + 81,  10))
			subtree:add(onboard5,      buffer(offset + 91,  10))
			subtree:add(onboard6,      buffer(offset + 101, 10))
			subtree:add(onboard7,      buffer(offset + 111, 10))
			subtree:add(onboard8,      buffer(offset + 121, 10))
			subtree:add(onboard9,      buffer(offset + 131, 10))
			subtree:add(onboard10,     buffer(offset + 141, 10))
			subtree:add(jrgb2,         buffer(offset + 151, 10))
			subtree:add(save,          buffer(offset + 161,  1))
		end
	else
		-- handle direct LED requests
		local subtree = tree:add(usb_ml_protocol, buffer(7), "USB Mystic Light 185 direct")
	  
		subtree:add(requestType, buffer(offset,        1))
		subtree:add(header,      buffer(offset + 1,    4))
		subtree:add(led0_4,      buffer(offset + 5,   15))
		subtree:add(led5_9,      buffer(offset + 20,  15))
		subtree:add(led10_14,    buffer(offset + 35,  15))
		subtree:add(led15_19,    buffer(offset + 50,  15))
		subtree:add(led20_24,    buffer(offset + 65,  15))
		subtree:add(led25_29,    buffer(offset + 80,  15))
		subtree:add(led30_34,    buffer(offset + 95,  15))
		subtree:add(led35_39,    buffer(offset + 110, 15))
		subtree:add(led40_44,    buffer(offset + 125, 15))
		subtree:add(led45_49,    buffer(offset + 140, 15))
		subtree:add(led50_54,    buffer(offset + 155, 15))
		subtree:add(led55_59,    buffer(offset + 170, 15))
		subtree:add(led60_64,    buffer(offset + 185, 15))
		subtree:add(led65_69,    buffer(offset + 200, 15))
		subtree:add(led70_74,    buffer(offset + 215, 15))
		subtree:add(led75_79,    buffer(offset + 230, 15))
		subtree:add(led80_84,    buffer(offset + 245, 15))
		subtree:add(led85_89,    buffer(offset + 260, 15))
		subtree:add(led90_94,    buffer(offset + 275, 15))
		subtree:add(led95_99,    buffer(offset + 290, 15))
		subtree:add(led100_104,  buffer(offset + 305, 15))
		subtree:add(led105_109,  buffer(offset + 320, 15))
		subtree:add(led110_114,  buffer(offset + 335, 15))
		subtree:add(led115_119,  buffer(offset + 350, 15))
		subtree:add(led120_124,  buffer(offset + 365, 15))
		subtree:add(led125_129,  buffer(offset + 380, 15))
		subtree:add(led130_134,  buffer(offset + 395, 15))
		subtree:add(led135_139,  buffer(offset + 410, 15))
		subtree:add(led140_144,  buffer(offset + 425, 15))
		subtree:add(led145_149,  buffer(offset + 440, 15))
		subtree:add(led150_154,  buffer(offset + 455, 15))
		subtree:add(led155_159,  buffer(offset + 470, 15))
		subtree:add(led160_164,  buffer(offset + 485, 15))
		subtree:add(led165_169,  buffer(offset + 500, 15))
		subtree:add(led170_174,  buffer(offset + 515, 15))
		subtree:add(led175_179,  buffer(offset + 530, 15))
		subtree:add(led180_184,  buffer(offset + 545, 15))
		subtree:add(led185_189,  buffer(offset + 560, 15))
		subtree:add(led190_194,  buffer(offset + 575, 15))
		subtree:add(led195_199,  buffer(offset + 590, 15))
		subtree:add(led200_204,  buffer(offset + 605, 15))
		subtree:add(led205_209,  buffer(offset + 620, 15))
		subtree:add(led210_214,  buffer(offset + 635, 15))
		subtree:add(led215_219,  buffer(offset + 650, 15))
		subtree:add(led220_224,  buffer(offset + 665, 15))
		subtree:add(led225_229,  buffer(offset + 680, 15))
		subtree:add(led230_234,  buffer(offset + 695, 15)) 
		subtree:add(led235_239,  buffer(offset + 710, 15))
	end
end

DissectorTable.get("usb.control"):add(3, usb_ml_protocol)
DissectorTable.get("usb.control"):add(9, usb_ml_protocol)
DissectorTable.get("usb.control"):add(0xffff, usb_ml_protocol)
